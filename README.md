# LootApp

This library helps any adventurer to keep all the loot organized like a true master of magic ✨🧙‍♂️✨

This project implements a solution for [Bags kata](https://katalyst.codurance.com/bags) created by [Giulio Perrone](https://github.com/pepperrone).

## Prerequisites

Before you begin, ensure you have met the following requirements:

* You have installed the latest version of `PHP`
* You have installed the latest version of `composer`

## Installation

To install <project_name>, follow these steps:

1. Init your composer project with: `composer init`
2. Add the path to the repository in the composer repository list
```
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/jaume.uep/bags-kata"
    }
],
```
3. Install the repository with `$ composer require jaume.uep/bags-kata`

## Basic Usage

```
<?php
require 'vendor/autoload.php';

use JaumeNicolau\Bags\LootApp;
use JaumeNicolau\Bags\Bag;

// create a lootApp instance
$lootApp = new LootApp();

// add additional containers with categories
$lootApp->add(new Bag('Metals'));

// add items to the inventory
$lootApp->addItem('Iron');
$lootApp->addItem('Leather');

// sort the inventory
$lootApp->sort();
```

## Contributing to LootApp
To contribute to LootApp, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name>/<location>`
5. Create the pull request.


## Contributors

Thanks to the following people who have contributed to this project:

* [@jaume.uep](https://gitlab.com/jaume.uep)

## Contact

If you want to contact me you can reach me at <jaume.uep@gmail.com>.
