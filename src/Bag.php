<?php

namespace JaumeNicolau\Bags;

class Bag extends Container
{
    /**
     * Bag's taxonomy.
     * @var string
     */
    protected string $category;

    /**
     * Creates a new Bag.
     * @param string $category The category to assign to the new Bag.
     * @return Bag
     */
    public function __construct(string $category = '')
    {
        $this->capacity = 4;
        $this->category = $category;
    }

    /**
     * Returns the Bag category.
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }
}
