<?php

namespace JaumeNicolau\Bags;

use JaumeNicolau\Bags\Exceptions\InventoryFullException;
use JaumeNicolau\Bags\Interfaces\ContainerInterface;

abstract class Container implements ContainerInterface
{
    /**
     * Maximum number of items the container can have.
     * @var int $capacity
     */
    protected int $capacity = 0;

    /**
     * Container list of items.
     * @var string $inventory
     */
    protected array $inventory = [];

    /**
     * Adds an item to the Container internal inventory.
     * @throws InventoryFullException if there's no available capacity in the inventory.
     * @param string $value The value to add.
     * @return void
     */
    public function addItem(string $value): void
    {
        if ($this->isInventoryFull()) {
            throw new InventoryFullException(self::class . " inventory full");
        }
        $this->inventory[] = $value;
    }

    public function isInventoryFull(): bool
    {
        return (count($this->inventory) >= $this->capacity);
    }

    public function getCapacity(): int
    {
        return $this->capacity;
    }

    public function getInventory(): array
    {
        return $this->inventory;
    }

    public function emptyInventory(): void
    {
        $this->inventory = [];
    }

    public function sort(): void
    {
        sort($this->inventory);
    }
}
