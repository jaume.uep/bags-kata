<?php

namespace JaumeNicolau\Bags;

class Backpack extends Container
{
    /**
     * Creates a new Backpack.
     * @return Backpack
     */
    public function __construct()
    {
        $this->capacity = 8;
    }
}
