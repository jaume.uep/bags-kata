<?php

namespace JaumeNicolau\Bags;

use JaumeNicolau\Bags\Exceptions\AllContainersFullException;
use JaumeNicolau\Bags\Interfaces\ContainerInterface;

class LootApp
{
    /**
     * App main Container or backpack.
     * @var Backpack $backpack
     */
    protected Backpack $backpack;

    /**
     * List of secondary Containers or bags.
     * @var array $bags
     */
    protected array $bags;

    /**
     * Relationship between items and its categories.
     * @var array $itemCategoryMap
     */
    private array $itemCategoryMap = [
        'Leather' => 'Clothes',
        'Linen' => 'Clothes',
        'Silk' => 'Clothes',
        'Wool' => 'Clothes',
        'Copper' => 'Metals',
        'Gold' => 'Metals',
        'Iron' => 'Metals',
        'Silver' => 'Metals',
        'Axe' => 'Weapons',
        'Dagger' => 'Weapons',
        'Mace' => 'Weapons',
        'Sword' => 'Weapons',
        'Cherry Blossom' => 'Herbs',
        'Marigold' => 'Herbs',
        'Rose' => 'Herbs',
        'Seaweed' => 'Herbs',
    ];

    /**
     * Creates a new LootApp.
     * @return LootApp
     */
    public function __construct()
    {
        $this->backpack = new Backpack();
        $this->bags = [];
    }

    /**
     * Returns the list of secondary Containers.
     * @return LootApp
     */
    public function getBags(): array
    {
        return $this->bags;
    }

    /**
     * Returns the main Container.
     * @return LootApp
     */
    public function getBackpack(): Backpack
    {
        return $this->backpack;
    }

    /**
     * Adds a new Bag to the list of secondary Containers.
     * @param Bag $bag The new Container to add.
     * @return void
     */
    public function addBag(Bag $bag): void
    {
        $this->bags[] = $bag;
    }

    /**
     * Adds a new item to the first available Container.
     * @param string $item The new item to add.
     * @throws AllContainersFullException if there's no available container.
     * @return void
     */
    public function addItem(string $item): void
    {
        $container = $this->getAvailableContainer();
        if (!$container) {
            throw new AllContainersFullException('All containers are full');
        }

        $container->addItem($item);
    }

    /**
     * Returns the first not full Container.
     * The main container is always checked first. If it's full, it will search in the secondary container list.
     * @return ContainerInterface|null
     */
    public function getAvailableContainer() : ?ContainerInterface {
        if (!$this->backpack->isInventoryFull()) {
            return $this->backpack;
        }

        foreach ($this->bags as $bag) {
            if (!$bag->isInventoryFull()) {
                return $bag;
            }
        }

        return null;
    }

    /**
     * Returns the first not full Container with the indicated category.
     * Since the main container doesn't have category, this method only checks the secondary container list.
     * @return ContainerInterface|null
     */
    public function getAvailableContainerByCategory(string $category) : ?ContainerInterface {
        foreach ($this->bags as $bag) {
            if (!$bag->isInventoryFull() && $bag->getCategory() == $category) {
                return $bag;
            }
        }

        return null;
    }

    /**
     * Sorts all the internal containers.
     * After sorting the items, each bag should have the items belonging to its category, sorted alphabetically, filling the main container first.
     * @return void
     */
    public function sort() :void
    {
        $items = [$this->backpack->getInventory()];
        $this->backpack->emptyInventory();

        foreach ($this->bags as $bag) {
            $items[] = $bag->getInventory();
            $bag->emptyInventory();
        }

        $items = array_merge(...$items);
        sort($items);

        foreach ($items as $item) {
            $container = null;

            $category = $this->itemCategoryMap[$item];

            if ($category) {
                $container = $this->getAvailableContainerByCategory($category);
            }

            if (!$container) {
                $container = $this->getAvailableContainer();
            }

            if (!$container) {
                throw new AllContainersFullException('All containers are full');
            }

            $container->addItem($item);
            $container->sort();
        }
    }
}
