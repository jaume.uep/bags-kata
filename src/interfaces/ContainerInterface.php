<?php

namespace JaumeNicolau\Bags\Interfaces;


interface ContainerInterface
{
    /**
     * Adds an item to the Container internal inventory.
     * @param string $value The value to add.
     * @return void
     */
    public function addItem(string $value): void;

    /**
     * Checks if the Container internal inventory is equals or greater than its max capacity.
     * @return bool
     */
    public function isInventoryFull(): bool;

    /**
     * Returns the Container internal inventory max capacity.
     * @return int
     */
    public function getCapacity(): int;

    /**
     * Returns the Container internal inventory.
     * @return array
     */
    public function getInventory(): array;

    /**
     * Empties the Container internal inventory.
     * @return array
     */
    public function emptyInventory(): void;

    /**
     * Sorts alphabetically the Container internal inventory.
     * @return array
     */
    public function sort(): void;
}
