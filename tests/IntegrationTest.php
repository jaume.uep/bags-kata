<?php

declare(strict_types=1);

use JaumeNicolau\Bags\Backpack;
use JaumeNicolau\Bags\Bag;
use JaumeNicolau\Bags\Exceptions\InventoryFullException;
use JaumeNicolau\Bags\LootApp;
use PHPUnit\Framework\TestCase;

final class IntegrationTest extends TestCase
{
    public function testContainersAreInitiallyEmpty()
    {
        $bag = new Bag();
        $backpack = new Backpack();

        $this->assertEmpty($bag->getInventory());
        $this->assertEmpty($backpack->getInventory());
    }

    /**
     * @depends testContainersAreInitiallyEmpty
     */
    public function testContainersCapacity()
    {
        $bag = new Bag();
        $backpack = new Backpack();

        $this->assertSame(4, $bag->getCapacity());
        $this->assertSame(8, $backpack->getCapacity());
    }

    public function testBagCategoryIsAssigned()
    {
        $category = 'Metal';
        $bag = new Bag($category);

        $this->assertSame($category, $bag->getCategory());
    }

    /**
     * @depends testContainersCapacity
     */
    public function testContainersAddSingleItem()
    {
        $item = 'Iron';
        $expected = [$item];
        $bag = new Bag();
        $backpack = new Backpack();

        $bag->addItem($item);
        $backpack->addItem($item);

        $this->assertSame($expected, $bag->getInventory());
        $this->assertSame($expected, $backpack->getInventory());
        $this->assertFalse($bag->isInventoryFull());
        $this->assertFalse($backpack->isInventoryFull());
    }

    /**
     * @depends testContainersAddSingleItem
     */
    public function testContainersInventoryOverflow()
    {
        $this->expectException(InventoryFullException::class);

        $bag = new Bag();

        for ($i = 0; $i < $bag->getCapacity() + 1; $i++) {
            $bag->addItem('Item nº' . $i);
        }
    }

    /**
     * @depends testContainersInventoryOverflow
     */
    public function testContainerInventoryOverflowResult()
    {
        try {
            $backpack = new Backpack();

            for ($i = 0; $i < $backpack->getCapacity() + 1; $i++) {
                $backpack->addItem('Item nº' . $i);
            }
        } catch (InventoryFullException $e) {
            $this->assertSame($backpack->getCapacity(), count($backpack->getInventory()));
            $this->assertTrue($backpack->isInventoryFull());
        }
    }

    public function testAddItemToFullBackpackGoesIntoBag()
    {
        $item = 'Item';
        $overflowItem = 'Overflow Item';

        $expectedBag = new Bag();
        $expectedBag->addItem($overflowItem);

        $expectedBackpack = new Backpack();
        for ($i = 0; $i < $expectedBackpack->getCapacity(); $i++) {
            $expectedBackpack->addItem($item);
        }

        $lootApp = new LootApp();
        $lootApp->addBag(new Bag());
        for ($i = 0; $i < $expectedBackpack->getCapacity(); $i++) {
            $lootApp->addItem($item);
        }
        $lootApp->addItem($overflowItem);

        $this->assertEquals($expectedBackpack, $lootApp->getBackpack());
        $this->assertTrue($lootApp->getBackpack()->isInventoryFull());
        $this->assertEquals($expectedBag, $lootApp->getBags()[0]);
        $this->assertFalse($lootApp->getBags()[0]->isInventoryFull());
    }

    /**
     * @depends testAddItemToFullBackpackGoesIntoBag
     */
    public function testAddItemToFullBagGoesIntoOtherBag()
    {
        $item = 'Item';
        $overflowItem = 'Overflow Item';

        $lootApp = new LootApp();
        $lootApp->addBag(new Bag());
        $lootApp->addBag(new Bag());

        $expectedBackpack = new Backpack();
        for ($i = 0; $i < $expectedBackpack->getCapacity(); $i++) {
            $expectedBackpack->addItem($item);
            $lootApp->addItem($item);
        }

        $expectedFullBag = new Bag();
        for ($i = 0; $i < $expectedFullBag->getCapacity(); $i++) {
            $expectedFullBag->addItem($item);
            $lootApp->addItem($item);
        }

        $expectedBag = new Bag();
        $expectedBag->addItem($overflowItem);
        $lootApp->addItem($overflowItem);

        $this->assertEquals($expectedBackpack, $lootApp->getBackpack());
        $this->assertTrue($lootApp->getBackpack()->isInventoryFull());

        $this->assertEquals($expectedFullBag, $lootApp->getBags()[0]);
        $this->assertTrue($lootApp->getBags()[0]->isInventoryFull());

        $this->assertEquals($expectedBag, $lootApp->getBags()[1]);
        $this->assertFalse($lootApp->getBags()[1]->isInventoryFull());
    }

    /**
     * @depends testAddItemToFullBagGoesIntoOtherBag
     */
    public function testSortingSpell()
    {
        $items = ['Leather', 'Iron', 'Copper', 'Marigold', 'Wool', 'Gold', 'Silk', 'Copper', 'Copper', 'Cherry Blossom'];

        $lootApp = new LootApp();
        $lootApp->addBag(new Bag('Metals'));
        foreach ($items as $item) {
            $lootApp->addItem($item);
        }

        $lootApp->sort();

        $expectedBackpackInventory = ['Cherry Blossom', 'Iron', 'Leather', 'Marigold', 'Silk', 'Wool'];
        $expectedBagInventory = ['Copper', 'Copper', 'Copper', 'Gold'];
        $this->assertEquals($expectedBackpackInventory, $lootApp->getBackpack()->getInventory());
        $this->assertEquals($expectedBagInventory, $lootApp->getBags()[0]->getInventory());
    }
}
